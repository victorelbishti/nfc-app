export interface IResponse<T> {
    data: T;
    executionTime: string;
    processId: string;
}