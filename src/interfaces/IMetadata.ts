export interface IMetadata {
    id: string;
    action?: string;
    name?: string;
    phone?: string;
    rentedBy?: string;
    rentedTime?: string;
    rentedPrice?: string;
}