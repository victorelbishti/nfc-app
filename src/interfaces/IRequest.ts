export interface IRequest<T> {
    workflowId: string;
    metadata: T;
    triggerNames: string;
}