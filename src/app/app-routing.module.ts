import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { BuyPageComponent } from './buy-page/buy-page.component';
import { ProductComponent } from './product/product.component';
import { RentPageComponent } from './rent-page/rent-page.component';
import { TransferPageComponent } from './transfer-page/transfer-page.component';

const routes: Route[] =[
    { path: 'product', component: ProductComponent },
    { path: 'rent', component: RentPageComponent },
    { path: 'transfer', component: TransferPageComponent },
    { path: 'buy', component: BuyPageComponent }
];

@NgModule({
    exports:[
        RouterModule,
    ],
    imports:[
        RouterModule.forRoot(routes)
    ]
})
export class AppRoutingModule{}