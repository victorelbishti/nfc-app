import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IMetadata } from 'src/interfaces/IMetadata';
import { OwnerService } from 'src/services/owner.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  title = 'well-being';
  productId: string = 'dbfa37d7-91b9-4a52-aa0e-b057d5ff89cd';
  owner: string;
  product: string;
  productCategory: string;
  ownerForm: FormGroup;
  fullPrice: string = "100 SEK";
  loader: boolean = false;
  confirmation: boolean = false;
  payment: boolean = false;
  action: string = "lend";

  constructor(private ownerService: OwnerService, private route: ActivatedRoute) {
    this.ownerForm = new FormGroup({
      reqOwner: new FormControl(''),
      reqPhone: new FormControl(''),
      reqDays: new FormControl()
    });

    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.productId = params['productId'];
    });
   }

   updatePrice(){
    var price = this.ownerForm.controls["reqDays"].value * 100;
    this.fullPrice = price + " SEK";
   }

   getOwner(){
    var metadata = {
      id: this.productId,
    } as IMetadata;
    this.ownerService.getOwner(metadata).subscribe(result => {
      this.owner = result.data.owner;
      this.product = result.data.productName;
      this.productCategory = result.data.productCategory;
      console.log(this.owner);      
    });
  }

  requestRentToOwner(){

    this.loader = true;

    setTimeout(() => {
      this.loader = false;
      this.confirmation = true;

      setTimeout(() => {
        this.confirmation = false;
        this.loader = true;

        setTimeout(() => {
          this.loader = false;
          this.payment = true;
          this.confirmation = false;
        }, 2000);
      }, 8000);
    }, 2000);

    var metadata = {
      id: this.productId,
      action: "rent",
      rentedBy: this.ownerForm.controls['reqOwner'].value,
      phone: this.ownerForm.controls['reqPhone'].value,
      rentedDays: this.ownerForm.controls['reqDays'].value,
      rentedPrice: this.fullPrice
    } as IMetadata;

      this.ownerService.requestOwner(metadata);
  }

  getName(){
    console.log(this.ownerForm.value);
    console.log(this.ownerForm.controls['reqPhone'].value);
  }

  ngOnInit(): void {
    this.getOwner();
  }

}
