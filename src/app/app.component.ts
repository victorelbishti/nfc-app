import { Component } from '@angular/core';
import { IMetadata } from 'src/interfaces/IMetadata';
import { OwnerService } from 'src/services/owner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'well-being';
  productId: string = 'dbfa37d7-91b9-4a52-aa0e-b057d5ff89cd';
  productName: string;
  owner: string;
  rentedBy: string;
  rentedTime: string;

  constructor(private ownerService: OwnerService)
  {
    this.getOwner();
  }

  requestOwner(){
    this.ownerService.requestOwner( { id: this.productId } as IMetadata);
  }

  getOwner(){
    this.ownerService.getOwner({ id: this.productId } as IMetadata).subscribe(result => {
      this.owner = result.data.owner;
      this.rentedBy = result.data.rentedBy;
      this.rentedTime = result.data.rentedTime;
      this.productName = result.data.productName;
    });
  }

  setOwner(){

    var phone = "0768361180";
    var name = "Victor Bi";

    var metadata = {
      id: this.productId,
      action: "transfer",
      phone,
      name
    } as IMetadata;

    this.ownerService.setOwner(metadata).subscribe(res => {
      this.getOwner();
    });
  }
}
