import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IMetadata } from 'src/interfaces/IMetadata';
import { OwnerService } from 'src/services/owner.service';

@Component({
  selector: 'app-transfer-page',
  templateUrl: './transfer-page.component.html',
  styleUrls: ['./transfer-page.component.scss']
})
export class TransferPageComponent implements OnInit {
  productId: string = 'dbfa37d7-91b9-4a52-aa0e-b057d5ff89cd';
  rentedBy: string;
  rentedPhone: string;
  productName: string;
  productCategory: string;
  loader: boolean = false;
  showConfirmation: boolean = false;

  constructor(private ownerService: OwnerService, private route: ActivatedRoute) {

    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.productId = params['productId'];
      this.getData();
    });
  }

  ngOnInit(): void {
  }

  getData(){
    this.ownerService.getOwner({ id: this.productId } as IMetadata).subscribe(result => {
      console.log(result);
      this.rentedBy = result.data.rentedBy;
      this.rentedPhone = result.data.rentedPhone;
      this.productName = result.data.productName;
      this.productCategory = result.data.productCategory;
    });
  }

  confirm(){
    console.log("Confirm");
    this.loader = true;
    
    var metadata = {
      id: this.productId,
      action: "transfer",
      name: this.rentedBy,
      phone: this.rentedPhone
    } as IMetadata;

    this.ownerService.setOwner(metadata).subscribe(res => {
      console.log(res);

      setTimeout(() => {
        this.loader = false;
        this.showConfirmation = true;
      }, 2000);
    });
  }
}
