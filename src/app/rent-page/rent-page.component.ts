import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IMetadata } from 'src/interfaces/IMetadata';
import { OwnerService } from 'src/services/owner.service';

@Component({
  selector: 'app-rent-page',
  templateUrl: './rent-page.component.html',
  styleUrls: ['./rent-page.component.scss']
})
export class RentPageComponent implements OnInit {
  productId: string = 'dbfa37d7-91b9-4a52-aa0e-b057d5ff89cd';
  rentedBy: string;
  rentedDays: string;
  rentedPrice: string;
  productName: string;
  productCategory: string;
  loader: boolean = false;
  showConfirmation: boolean = false;
  payed: boolean = false;

  constructor(private ownerService: OwnerService, private route: ActivatedRoute) {

    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.productId = params['productId'];
      this.getData();
    });
  }

  ngOnInit(): void {
  }

  getData(){
    this.ownerService.getOwner({ id: this.productId } as IMetadata).subscribe(result => {
      console.log(result);
      this.rentedBy = result.data.rentedBy;
      this.rentedDays = result.data.rentedDays;
      this.productName = result.data.productName;
      this.productCategory = result.data.productCategory;
      this.rentedPrice = result.data.rentedPrice;
    });
  }

  approve(){
    var metadata = {
      id: this.productId,
      action: "rent",
      rentedBy: this.rentedBy,
      rentedTime: this.formatDate()
    } as IMetadata;

    this.ownerService.setOwner(metadata).subscribe(res => {
      console.log(res);

      this.loader = true;
      setTimeout(() => {
        this.loader = false;
        this.showConfirmation = true;

        setTimeout(() => {
          this.loader = true;

          setTimeout(() => {
            this.payed = true;
          }, 5000);
        }, 3000);
      }, 1500);
    });
  }

  formatDate() {
    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = d.getHours(),
        min = d.getMinutes();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
  }
}
