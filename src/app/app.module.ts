import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { OwnerComponent } from './owner/owner.component';
import { ProductComponent } from './product/product.component';
import { OwnerService } from 'src/services/owner.service';
import { RentPageComponent } from './rent-page/rent-page.component';
import { HeaderComponent } from './header/header.component';
import { StatusComponent } from './status/status.component';
import { TransferPageComponent } from './transfer-page/transfer-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LendItemComponent } from './lend-item/lend-item.component';
import { ChoosePaymentComponent } from './choose-payment/choose-payment.component';
import { BuyPageComponent } from './buy-page/buy-page.component';

@NgModule({
  declarations: [
    AppComponent,
    OwnerComponent,
    ProductComponent,
    RentPageComponent,
    HeaderComponent,
    StatusComponent,
    TransferPageComponent,
    LendItemComponent,
    ChoosePaymentComponent,
    BuyPageComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    OwnerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
