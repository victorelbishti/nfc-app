import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IMetadata } from 'src/interfaces/IMetadata';
import { OwnerService } from 'src/services/owner.service';

@Component({
  selector: 'app-buy-page',
  templateUrl: './buy-page.component.html',
  styleUrls: ['./buy-page.component.scss']
})
export class BuyPageComponent implements OnInit {
  productId: string;
  owner: string;
  product: string;
  productCategory: string;
  ownerForm: FormGroup;
  fullPrice: string = "100 SEK";
  loader: boolean = false;
  confirmation: boolean = false;
  payment: boolean = false;
  action: string = "buy"; 

  constructor(private ownerService: OwnerService, private route: ActivatedRoute) {
    this.ownerForm = new FormGroup({
      reqOwner: new FormControl(''),
      reqPhone: new FormControl('')
    });

    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.productId = params['productId'];
    });
   }   

  ngOnInit(): void {
    this.getOwner();
  }

   updatePrice(){
    var price = this.ownerForm.controls["reqDays"].value * 100;
    this.fullPrice = price + " SEK";
   }

   getOwner(){
    var metadata = {
      id: this.productId,
    } as IMetadata;
    this.ownerService.getOwner(metadata).subscribe(result => {
      this.owner = result.data.owner;
      this.product = result.data.productName;
      this.productCategory = result.data.productCategory;
      console.log(this.owner);      
    });
  }

  requestRentToOwner(){

    this.loader = true;

    setTimeout(() => {
      this.loader = false;
      this.confirmation = true;

      setTimeout(() => {
        this.confirmation = false;
        this.loader = true;

        setTimeout(() => {
          this.loader = false;
          this.payment = true;
          this.confirmation = false;
        }, 2000);
      }, 8000);
    }, 2000);

    var metadata = {
      id: this.productId,
      action: "transfer",
      rentedPhone: this.ownerForm.controls['reqPhone'].value,
      rentedBy: this.ownerForm.controls['reqOwner'].value,
    } as IMetadata;

    this.ownerService.requestOwner(metadata);
  }
}
