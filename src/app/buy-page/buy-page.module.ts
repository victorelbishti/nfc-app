import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Route } from '@angular/router';
import { BuyPageComponent } from './buy-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const routes: Route[] =[
  { path: '', component: BuyPageComponent  }
];

@NgModule({
  declarations: [
    BuyPageComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    FormsModule
  ],
})
export class BuyPageModule { }
