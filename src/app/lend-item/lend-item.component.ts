import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-lend-item',
  templateUrl: './lend-item.component.html',
  styleUrls: ['./lend-item.component.css']
})
export class LendItemComponent implements OnInit {

  @Input() item: string;  
  @Input() action: string;
  
  @Output() event = new EventEmitter();

  constructor() { }
  
  ngOnInit(): void {
  }

  sendRequest(){
    this.event.emit();
  }

}
