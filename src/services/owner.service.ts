import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { IMetadata } from "src/interfaces/IMetadata";
import { IRequest } from "src/interfaces/IRequest";
import { IResponse } from "src/interfaces/IResponse";


@Injectable({
    providedIn: 'root'
})
export class OwnerService {

  // product id = 'dbfa37d7-91b9-4a52-aa0e-b057d5ff89cd'
  private readonly BaseUrl: string;
  private readonly WorkflowId: string;
  private readonly ApiKey: string;
  private readonly Headers: HttpHeaders;
  
  constructor(private httpClient: HttpClient) {
    this.BaseUrl = 'https://rest.bosbec.io/2/workflows/';
    this.WorkflowId = '78e0c392-dd10-4e4e-a627-3181427d0c07';
    this.ApiKey = 'eab89d49-fbb4-43d8-8e24-58e5a3ad56b4';
    this.Headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('api-key', this.ApiKey);
  }

  getOwner(metadata: IMetadata): Observable<IResponse<IGetOwnerData>> 
  {
    const trigger = 'GetOwner';

    var request = {
      workflowId: this.WorkflowId,
      metadata: metadata,
      triggerNames: trigger,
      requestSettings: {
        responseData: {
          owner: 'metadata.owner',
          rentedBy: 'metadata.rented_by',
          rentdedTime: 'metadata.rented_time',
          productName: 'metadata.product_name',
          rentedDays: 'metadata.rented_days',
          rentedPhone: 'metadata.rented_phone',
          productCategory: 'metadata.product_category',
          rentedPrice: 'metadata.rented_price'
        }
      }
    }

    return this.httpClient.post<IResponse<IGetOwnerData>>(this.BaseUrl, request, { headers: this.Headers });
  }

  requestOwner(metadata: IMetadata) {

    const trigger = 'RequestOwner';

    var request = {
      workflowId: this.WorkflowId,
      metadata: metadata,
      triggerNames: trigger
    } as IRequest<IMetadata>;

    this.httpClient.post(this.BaseUrl, request, { headers: this.Headers }).subscribe(result => {
      console.log(result);
    });
  }

  setOwner(metadata: IMetadata) {
    const trigger = 'SetData';

    var request = {
      workflowId: this.WorkflowId,
      metadata: metadata,
      triggerNames: trigger
    } as IRequest<IMetadata>;
    
    return this.httpClient.post(this.BaseUrl, request, { headers: this.Headers });
  }  
}

export interface IGetOwnerData {
  owner: string;
  rentedBy: string;
  rentedTime: string;
  productCategory: string;
  rentedDays: string;
  rentedPhone: string;
  productName: string;
  rentedPrice: string;
}